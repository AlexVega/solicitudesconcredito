import { Component, OnInit } from '@angular/core';
import { Prospecto } from '../Clases/prospecto';
import { SolicitudesService } from '../services/solicitudes.service';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { Renderer2, ElementRef } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-captura-prospecto',
  templateUrl: './captura-prospecto.component.html',
  styleUrls: ['./captura-prospecto.component.css'],
  providers: [SolicitudesService]
})
export class CapturaProspectoComponent implements OnInit {

  formCaptura:Prospecto = new Prospecto();

  constructor(private api:SolicitudesService, private location:Location,private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
  
  }

  guardar()
  {
    console.log(this.formCaptura);
    Swal.fire({
      title: '¿Cuenta con datos correctos?',
      text: "Al aceptar se guardará toda la información",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, deseo continuar'
    }).then((result) => {

      if(this.formCaptura.nombre && this.formCaptura.apellidopaterno && this.formCaptura.calle && this.formCaptura.numero && this.formCaptura.colonia && this.formCaptura.codigo_postal && this.formCaptura.telefono && this.formCaptura.RFC && this.formCaptura.documentos){
      if (result.value) {        
        const data = this.api.crearPostulante(this.formCaptura);
        Swal.fire(
          'Finalizado',
          'La captura se ha almacenado'
        )
      }
      window.location.href = "../listado";
    } else {
      Swal.fire({
        title: 'Los campos con un * son obligatorios, favor de capturar la información solicitada',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      })
    }
    }
    
    )
    
  } 

  salir(){
    Swal.fire({
      title: '¿Estás seguro?',
      text: "Al salir se perderá toda la información capturada. ¿Deseas continuar?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, deseo salir'
    }).then((result) => {
      window.location.href = "../listado";
      if (result.value) {
      
      }
    })
  }

  onChange(event: any) {
    let files = [].slice.call(event.target.files);
  }

  base64textString = [];

  onUploadChange(evt: any) {
    const file = evt.target.files[0];
  
    if (file) {
      const reader = new FileReader();
  
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }
  
  handleReaderLoaded(e) {
    this.base64textString.push('data:image/png;base64,' + btoa(e.target.result));
  }

}
