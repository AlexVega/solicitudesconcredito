import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapturaProspectoComponent } from './captura-prospecto.component';

describe('CapturaProspectoComponent', () => {
  let component: CapturaProspectoComponent;
  let fixture: ComponentFixture<CapturaProspectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapturaProspectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapturaProspectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
