import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CapturaProspectoComponent } from './captura-prospecto/captura-prospecto.component';
import { SolicitudesService } from './services/solicitudes.service';
import { ListadoProspectoComponent } from './listado-prospecto/listado-prospecto.component';
import { DetalleProspectoComponent } from './detalle-prospecto/detalle-prospecto.component';
import { EvaluacionComponent } from './evaluacion/evaluacion.component';
import { DescripcionComponent } from './descripcion/descripcion.component';

@NgModule({
  declarations: [
    AppComponent,
    CapturaProspectoComponent,
    ListadoProspectoComponent,
    DetalleProspectoComponent,
    EvaluacionComponent,
    DescripcionComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [SolicitudesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
