import { Component, OnInit } from '@angular/core';
import { Routes, ActivatedRoute } from '@angular/router';
import { SolicitudesService } from '../services/solicitudes.service';
import { Prospecto } from '../Clases/prospecto';

@Component({
  selector: 'app-detalle-prospecto',
  templateUrl: './detalle-prospecto.component.html',
  styleUrls: ['./detalle-prospecto.component.css']
})
export class DetalleProspectoComponent implements OnInit {
  id:number;
  prospecto:Prospecto;

  constructor(private Routes: ActivatedRoute,private api:SolicitudesService) { }

  async ngOnInit() {
    await this.Routes.queryParams.subscribe(params=>{

      this.id = params["id"];
      this.ObtenerDatosProspecto();
    })
  }
  
  async ObtenerDatosProspecto(){
    const data = await this.api.obtenerProspectoDetalle(this.id);
    if (data.status == 200){

      this.prospecto= data.data;
    }else {
      alert("Ocurrió un error en el servicio");
    }
  }
  async Redireccionar(){
    window.location.href = "../listado";
  }

}
