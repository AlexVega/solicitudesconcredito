export class Prospecto {
    id:number = 0;
    nombre:string = "";
    apellidopaterno:string = "";
    apellidomaterno:string = "";
    calle:string = "";
    numero:number = 0;
    colonia:string = "";
    codigo_postal:number = 0;
    telefono:string = "";
    RFC:string = "";
    estatus:string ="ENVIADO";
    observaciones:string ="";
    documentos:string ="";
}
