import { Component, OnInit } from '@angular/core';
import { SolicitudesService } from '../services/solicitudes.service';
import { Prospecto } from '../Clases/prospecto';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listado-prospecto',
  templateUrl: './listado-prospecto.component.html',
  styleUrls: ['./listado-prospecto.component.css']
})
export class ListadoProspectoComponent implements OnInit {
  bandera3:boolean;
  prospectos: Array<Prospecto> = new Array<Prospecto>();
  
  constructor(private api:SolicitudesService, private location:Location) { }

  async ngOnInit() {
    this.bandera3 = false;
    const data = await this.api.ObtenerProspectos();
      if (data.status == 200){
        this.prospectos= data.data;
        console.log(this.prospectos[0].estatus); 
      }else {
        alert("Ocurrió un error en el servicio");
      }
      
  }
  Agregar(){
      window.location.href = "../captura";
  }

}



