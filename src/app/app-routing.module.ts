import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CapturaProspectoComponent } from './captura-prospecto/captura-prospecto.component';
import { ListadoProspectoComponent } from './listado-prospecto/listado-prospecto.component';
import { DetalleProspectoComponent } from './detalle-prospecto/detalle-prospecto.component';
import { EvaluacionComponent } from './evaluacion/evaluacion.component';
import { DescripcionComponent } from './descripcion/descripcion.component';


const routes: Routes = [{path:"captura", component: CapturaProspectoComponent},{path:"listado",component: ListadoProspectoComponent},
{path:"detalle",component:DetalleProspectoComponent},{path:"evaluacion",component:EvaluacionComponent},{path:"descripcion",component:DescripcionComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
