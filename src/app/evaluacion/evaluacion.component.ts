import { Component, OnInit } from '@angular/core';
import { SolicitudesService } from '../services/solicitudes.service';
import { Routes, ActivatedRoute } from '@angular/router';
import { Prospecto } from '../Clases/prospecto';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { equal } from 'assert';

@Component({
  selector: 'app-evaluacion',
  templateUrl: './evaluacion.component.html',
  styleUrls: ['./evaluacion.component.css']
})
export class EvaluacionComponent implements OnInit {
  id:number;
  formCaptura:Prospecto = new Prospecto();
  prospecto:Prospecto;
  bandera:boolean;
  bandera2:boolean;
  bandera3:boolean;
  data:object;
  prospectos: Array<Prospecto> = new Array<Prospecto>();

  constructor(private Routes: ActivatedRoute, private api:SolicitudesService) { }

  async ngOnInit() {  
    this.bandera2 = true;
/*     const data = await this.api.obtenerProspectoDetalle(this.id);
    this.prospecto = data.data;
    console.log(this.prospecto[0].observaciones);
    if(this.prospectos[0].estatus == "AUTORIZADO"){
    this.bandera3 = true;
    console.log(this.bandera3);
  } */

    await this.Routes.queryParams.subscribe(params=>{

    this.id = params["id"];
    this.ObtenerDatosProspecto();
  })
  }

  async ObtenerDatosProspecto(){
    const data = await this.api.obtenerProspectoDetalle(this.id);
    this.bandera = true;
    if (data.status == 200){
       this.prospecto = data.data;
       console.log(this.prospecto[0].observaciones);
       
       if (this.prospecto[0].observaciones){
        
        this.bandera = false;
        console.log(this.bandera);
        
       }
    }else {
      alert("Ocurrió un error en el servicio");
    }
  }

  async ActualizarProspecto(){
    console.log(this.formCaptura);
    if(!this.formCaptura.observaciones.length) {
      this.formCaptura.estatus = "AUTORIZADO";
      this.data = this.api.actualizar(this.id, this.formCaptura.estatus,this.formCaptura.observaciones);
    }else {
      this.formCaptura.estatus = "RECHAZADO";
      this.data = this.api.actualizar(this.id, this.formCaptura.estatus,this.formCaptura.observaciones);
    } 
    this.Redireccionar();
    return this.data;

    
  }
  async MostrarRechazo(){
    this.bandera2 = false;
  }
  async Redireccionar(){
    window.location.href = "../listado";
  }


}
