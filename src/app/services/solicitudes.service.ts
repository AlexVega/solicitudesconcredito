import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders } from '@angular/common/http'; 
import { environment } from '../../environments/environment';
import { RespuestaServicio } from '../Clases/respuesta-servicio';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';


@Injectable({
  providedIn: 'root'
})
export class SolicitudesService {

  constructor( private http: HttpClient) { }

  urlService = environment.urlService;
  host:string ="http://localhost:4200";
  body = {};
  respuesta = {};
  estatus = "";
  observaciones = "";
  


    ObtenerProspectos()
  {
    return this.http.get<RespuestaServicio>(`${this.urlService}/prospectos`).toPromise();
  }

  obtenerProspectoDetalle(id:number){
    return this.http.get<RespuestaServicio>(`${this.urlService}/listado/${id}`).toPromise();

  }

  obtenerProspectoDetalles(id:number){
    return this.http.get<RespuestaServicio>(`${this.urlService}/detalle/${id}`).toPromise();

  }

   crearPostulante(data:object){
    this.respuesta= this.http.post(`${this.urlService}/prospectos/`, data).toPromise();
    return this.respuesta;

  }  

  actualizar(id:number, estatus:string, observaciones:string){
    this.body = {estatus, observaciones}
    this.respuesta = this.http.put(`${this.urlService}/validacion/${id}`,this.body).toPromise();
    return this.respuesta;
  }
 
}


